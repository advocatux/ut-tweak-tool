/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import StorageManager 1.0
import GSettings 1.0
import Ubuntu.SystemSettings.Sound 1.0

import "../components"
import "../components/ListItems" as ListItems
import "../components/Dialogs"

// TODO: Show an audio trimmer when a file is picked
// TODO: Always create a new copy of the file in the internal storage.

Page {
    id: rootItem 
    anchors.fill: parent

    signal filePicked(var path)

    onFilePicked:{
	console.log(path)
    }

    header: PageHeader {
        id: audioPage
        title: i18n.tr("Message received (SMS ONLY)")
        flickable: view
    }

    Column {
        id: column

        width: view.width
        anchors.top: audioPage.bottom

    function getSettings() {
        messageSoundItem.subtitle.text = StorageManager.getFileFullName(settings.incomingMessageSound)
    }

    function setIncomingMessageSound(file) {
        settings.incomingMessageSound = file;
        backend.incomingMessageSound = file;

        getSettings();
    }

    Component.onCompleted: getSettings()

    ListItems.Warning {
        text: i18n.tr("Please note that if you choose an audio file from an external media (e.g. SD card), that file could not be played if the media will be removed.")
    }

    ListItems.SectionDivider { text: i18n.tr("Message received") }

    ListItems.Warning {
        iconName: "messages-new"
        text: i18n.tr("Ensure your audio file length is max. 10 seconds.<br><b>N.B.</b> The file will be played until its end: if you use a 3 minutes song, your device will be ringing for that time!")
    }

    ListItems.Button {
        id: messageSoundItem

        title.text: i18n.tr("Current sound:")
        button {
            text: i18n.tr("Pick")
            onClicked: {
                pageStack.addPageToCurrentColumn(rootItem, fileDialog)
            }
        }
    }

    ListItems.Base {
        Label {
            text: i18n.tr("Restore default...")
            anchors.verticalCenter: parent.verticalCenter
        }

        onClicked: {
            var dialog = PopupUtils.open(defaultDialog);
            dialog.accepted.connect(function() {
                column.setIncomingMessageSound("/usr/share/sounds/ubuntu/notifications/Xylo.ogg")
            })
        }
    }

    GSettings {
        id: settings
        schema.id: "com.ubuntu.touch.sound"
    }

    UbuntuSoundPanel { id: backend }

    Component {
        id: fileDialog

        FilePickerPage {
	    id: pickerPage

	    property var callback: function(){}

            title: i18n.tr("Choose a ringtone")
            folder: "file://" + StorageManager.getXdgFolder(StorageManager.HomeLocation)
            nameFilters: ["*.mp3", "*.ogg"]
            onAccepted: {
                column.setIncomingMessageSound(path.toString().replace("file://", ""))
                rootItem.pageStack.removePages(pickerPage)
            }
        }
    }

    Component {
        id: defaultDialog

        Dialog {
            id: defaultDialogue

            signal accepted

            title: i18n.tr("Restore default")
            text: i18n.tr("Are you sure?")

            Button {
                text: i18n.tr("Yes")
                color: UbuntuColors.orange
                onClicked: {
                    defaultDialogue.accepted();
                    PopupUtils.close(defaultDialogue);
                }
            }

            Button {
                text: i18n.tr("No")
                onClicked: PopupUtils.close(defaultDialogue);
            }
        }
      }
    }
  }
